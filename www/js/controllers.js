var starter=angular.module('starter.controllers', [])

starter.controller('LoginCtrl',['$scope','LoginService','$ionicPopup','$state','$cordovaSQLite', '$ionicSideMenuDelegate', '$window',
 function($scope, LoginService, $ionicPopup, $state, $cordovaSQLite, $ionicSideMenuDelegate, $window) {
    $scope.data = {};
    $scope.loginData = {};
    $ionicSideMenuDelegate.canDragContent(false);
    

    $scope.dologin = function() {
      if(($scope.loginData.fullname != undefined || $scope.loginData.fullname != "") 
            && ($scope.loginData.password != undefined || $scope.loginData.password != "")) {
       
        console.log("Entered:", $scope.loginData.fullname+" "+$scope.loginData.password);
        $cordovaSQLite.execute(db, 'SELECT * FROM user WHERE fullname=? and password=?', 
                                  [$scope.loginData.fullname,$scope.loginData.password])
        .then(function(result) {
          console.log("Login successful", result);
          if(result.rows.length > 0) {
            $scope.getMethod(result.rows.item(0).fullname);
            localStorage.setItem("user", result.rows.item(0).fullname);
            localStorage.setItem("user1", result.rows.item(0).RetailerCode);
            localStorage.setItem("user2", result.rows.item(0).TeritarySalesEntryDt);
            localStorage.setItem("user3", result.rows.item(0).FranchiseeCode);
            localStorage.setItem("user4", result.rows.item(0).FranchiseeName);
            console.log("Created:", localStorage.getItem("user2"));
            $state.go('dashboard');
            /*$window.location.reload();*/
          } else {
             var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
          }
          
        }, 
        function(error) {
          console.log("Error on saving: ", error.message);
        });
      }else{
        console.log("Error on saving: ", "please enter username and password !");
      }
    };

    $scope.getMethod = function(userId) {
      if(userId != undefined || userId != "")  {
        $cordovaSQLite.execute(db, 'SELECT * FROM CustomerDetail WHERE DealerName =? ',
                                  [userId])
        .then(function(result) {
          console.log("Get successful", result.rows);
          $window.location.reload();
            localStorage.setItem("CustomerDetails", JSON.stringify(result.rows));
         // $scope.customerDetails = (localStorage.getItem('CustomerDetails'));
         //console.log('$scope.customerDetails',JSON.parse($scope.customerDetails))
         // $scope.details = JSON.parse($scope.customerDetails);
         // console.log('details', $scope.details);
        }, 
        function(error) {
          console.log("Error on saving: ", error.message);
        });
      }else{
        console.log("Error on saving: ", "please enter username and password !");
      }
    };


}]);



starter.controller('mainPageCtrl', function($scope, $cordovaBarcodeScanner, $rootScope, $ionicSideMenuDelegate, $cordovaSQLite, $state, $window, $ionicPopup) {
    $scope.fullname = localStorage.getItem("user");
    $scope.RetailerCode = localStorage.getItem("user1");
    $scope.TeritarySalesEntryDt = localStorage.getItem("user2");
    $scope.FranchiseeCode = localStorage.getItem("user3");
    $scope.FranchiseeName = localStorage.getItem("user4");
    $scope.battery = localStorage.getItem("id1");
    $scope.ProductCode = localStorage.getItem("id2");
    /*var barcodescan = $state.params.battery;
    console.log("param",$state.params)*/
    
          $scope.customerDetails = (localStorage.getItem('CustomerDetails'));
          console.log('$scope.customerDetails',JSON.parse($scope.customerDetails))
          $scope.details = JSON.parse($scope.customerDetails);
          console.log('details', $scope.details);
          // console.log('detailsddss', Object.keys($scope.details).length);
           $scope.detailsLength = Object.keys($scope.details).length;
    $scope.scanBarcode = function() {
       $cordovaBarcodeScanner.scan().then( 
            function (result) {
                console.log("insss",result)
                $rootScope.barcoderesults = {
                    Result: result.text,
                    Format: result.format,
                    Cancelled: result.cancelled,
                    dt: new Date()
                };console.log("Barrrrr",result.text)
                $scope.BarcodegetMethod(result.text);
                 
            },  function(error) {
            console.log("An error happened -> " + error);
        });
    };

    $scope.BarcodegetMethod = function(userId) {
        console.log("user Scan ",userId)
      if(userId != undefined || userId != "")  {
        $cordovaSQLite.execute(db, 'SELECT * FROM ScanDetail WHERE BatterySerialNo=?',
                                  [userId])
        .then(function(result) {
          console.log("Get successful", result);
          if(result.rows.length > 0){
              localStorage.setItem("id1", result.rows.item(0).BatterySerialNo);
              localStorage.setItem("id2", result.rows.item(0).ProductCode);
                console.log("bbbbbbb:", localStorage.getItem("id2"));
                /*$state.go('mainPage', {'battery': result.rows[0].BatterySerialNo, 'productCode': result.rows[0].ProductCode}, {reload: true, notify:true});*/
                $state.go('mainPage');
                console.log("Battery",result.rows[0].BatterySerialNo)
                 console.log("productCode",result.rows[0].ProductCode)
          }else{
            var alertPopup = $ionicPopup.alert({
                title: 'Barcode failed!',
                template: 'Please check your Barcode!'
            });
          }
        }, 
        function(error) {
          console.log("Error on saving: ", error.message);
        });
      }else{
        console.log("Error on saving: ", "please enter username and password !");
      }
    };

    $scope.openMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };

    $scope.user = { CustomerName: '', CustomerPhoneNo: '', vehicleModel: '', vehicleMake: '', vehicleSegment: '', engineType: '', vehicleNo:'', remarks: '' };
    $scope.custDetail = function (CustomerName, CustomerPhoneNo, FranchiseeCode, FranchiseeName, vehicleModel, vehicleMake, vehicleSegment, engineType, vehicleNo, remarks, batterySerialNo, productCode) {
    $cordovaSQLite.execute(db, 'INSERT INTO CustomerDetail (DealerName, CustomerName, CustomerPhoneNo, FranchiseeCode, FranchiseeName, vehicleModel, vehicleMake, vehicleSegment, engineType, vehicleNo, remarks, BatterySerialNo, ProductCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                                            [$scope.fullname, $scope.user.CustomerName, $scope.user.CustomerPhoneNo, $scope.FranchiseeCode, $scope.FranchiseeName, $scope.user.vehicleModel, $scope.user.vehicleMake, $scope.user.vehicleSegment, $scope.user.engineType, $scope.user.vehicleNo, $scope.user.remarks, $scope.battery, $scope.ProductCode])
        .then(function() {
            $scope.getMethod($scope.fullname);
            $state.go('dashboard', null, {reload: true, notify:true});
            $scope.statusMessage = "Data saved successful, cheers!";
        }, function(error) {
            $scope.statusMessage = "Error on saving: " + error.message;
        })
    }



    $scope.IsVisible = false;
    $scope.ShowHide = function () {
        $scope.IsVisible = $scope.IsVisible ? false : true;
        $scope.IsVisibled = $scope.IsVisibled ? false : true;
    }

   $scope.getMethod = function(userId) {
      if(userId != undefined || userId != "")  {
        $cordovaSQLite.execute(db, 'SELECT * FROM CustomerDetail WHERE DealerName =? ',
                                  [userId])
        .then(function(result) {
          console.log("Get successful", result.rows);
          $window.location.reload();
          localStorage.setItem("CustomerDetails", JSON.stringify(result.rows));
          $scope.customerDetails = (localStorage.getItem('CustomerDetails'));
         //console.log('$scope.customerDetails',JSON.parse($scope.customerDetails))
          $scope.details = JSON.parse($scope.customerDetails);
          $scope.detailsLength = Object.keys($scope.details).length;
         // console.log('details', $scope.details);
        }, 
        function(error) {
          console.log("Error on saving: ", error.message);
        });
      }else{
        console.log("Error on saving: ", "please enter username and password !");
      }
    };

    $scope.logout= function(){
        $state.go('login');
        $window.location.reload();
    };

    $scope.devList =[
        {name: "Value1", id:"1"},
        {name: "Value2", id:"2"},
        {name: "Value3", id:"3"},
        {name: "Value4", id:"4"},
    ];

})

angular.module('$selectBox', []).directive('selectBox', function () {
    return {
        restrict: 'E',
        require: ['ngModel', 'ngData', 'ngSelectedId', 'ngSelectedValue', '?ngTitle', 'ngiItemName', 'ngItemId'],
        template: '<input id="showed" type="text" ng-click="showSelectModal()" style="cursor:inherit;" readonly />' + '<span id="hidden" type="text" style="display: none;"></span>',
        controller: function ($scope, $element, $attrs, $ionicModal, $parse) {
            $scope.modal = {};

            $scope.showSelectModal = function () {
                var val = $parse($attrs.ngData);
                $scope.data = val($scope);
              $scope.modal.show();
            };

            $scope.closeSelectModal = function () {
                $scope.modal.hide();
            };

            $scope.$on('$destroy', function (id) {
                $scope.modal.remove();
            });

            $scope.modal = $ionicModal.fromTemplate('<ion-modal-view id="select">' + '<ion-header-bar>' + '<h1 class="title">' + $attrs.ngTitle + '</h1>' + ' <a ng-click="closeSelectModal()" class="button button-icon icon ion-close"></a>' + '</ion-header-bar>' + '<ion-content>' + '<ion-list>' + '<ion-item  ng-click="clickItem(item)" ng-repeat="item in data" ng-bind-html="item[\'' + $attrs.ngItemName + '\']"></ion-item>' + '</ion-list>' + ' </ion-content>' + '</ion-modal-view>', {
                scope: $scope,
                animation: 'slide-in-up'
            });

            $scope.clickItem = function (item) {
                var index = $parse($attrs.ngSelectedId);
                index.assign($scope.$parent, item[$attrs.ngItemId]);

                var value = $parse($attrs.ngSelectedValue);
                value.assign($scope.$parent, item[$attrs.ngItemName]);
                $scope.closeSelectModal();
            };
        },
        compile: function ($element, $attrs) {
            var input = $element.find('input');
            angular.forEach({
                'name': $attrs.name,
                'placeholder': $attrs.ngPlaceholder,
                'ng-model': $attrs.ngSelectedValue
            }, function (value, name) {
                if (angular.isDefined(value)) {
                    input.attr(name, value);
                }
            });

            var span = $element.find('span');
            if (angular.isDefined($attrs.ngSelectedId)) {
                span.attr('ng-model', $attrs.ngSelectedId);
            }
        }
    };
});


