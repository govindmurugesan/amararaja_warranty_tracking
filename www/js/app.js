var db = null;
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var starter=angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', '$selectBox'])

starter.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  
    db = window.openDatabase("amaron.db","1", "SqliteDemo", "2000");
    //db = $cordovaSQLite.openDB("amaron.db","1", "SqliteDemo", "2000");
      $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS User(fullname TEXT, password TEXT, RetailerCode TEXT, TeritarySalesEntryDt Text, FranchiseeCode TEXT, FranchiseeName TEXT, UNIQUE(fullname) ON CONFLICT IGNORE)');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100101","123","S N Battery Point","18/01/2017" , "501001", "Rahul")');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100102","123","Adhitya Enterprise","18/01/2017", "501002", "Chethan")');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100103","123","Sri Manjunatha Batteries","18/01/2017", "501003", "Karthik")');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100104","123","Thirumala Batteries","18/01/2017", "501004", "Tamil")');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100105","123","SLN Sphere Solutions","18/01/2017", "501005", "Nathan")');
        $cordovaSQLite.execute(db, 'INSERT INTO User (fullname, password, RetailerCode, TeritarySalesEntryDt, FranchiseeCode, FranchiseeName) VALUES ("100106","123","Manju Sree Batteries","18/01/2017", "501006", "Azhar")');
      $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS CustomerDetail(ID INTEGER PRIMARY KEY   AUTOINCREMENT, DealerName TEXT, CustomerName TEXT, CustomerPhoneNo Text, FranchiseeCode TEXT, FranchiseeName TEXT, vehicleModel TEXT, vehicleMake TEXT, vehicleSegment TEXT, engineType TEXT, vehicleNo TEXT, remarks TEXT, batterySerialNo TEXT, productCode TEXT)');
      $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS ScanDetail(BatterySerialNo TEXT, ProductCode TEXT, UNIQUE(BatterySerialNo,ProductCode) ON CONFLICT IGNORE)');
        $cordovaSQLite.execute(db, 'INSERT INTO ScanDetail (BatterySerialNo, ProductCode) VALUES ("012345678905","AAM-BL-0BL600RMF")');
        $cordovaSQLite.execute(db, 'INSERT INTO ScanDetail (BatterySerialNo, ProductCode) VALUES ("059345678900","AAM-BL-0BL601RMF")');
        $cordovaSQLite.execute(db, 'INSERT INTO ScanDetail (BatterySerialNo, ProductCode) VALUES ("201234567899","AAM-BL-0BL602RMF")');
        $cordovaSQLite.execute(db, 'INSERT INTO ScanDetail (BatterySerialNo, ProductCode) VALUES ("640509040147","AAM-BL-0BL603RMF")');
        $cordovaSQLite.execute(db, 'INSERT INTO ScanDetail (BatterySerialNo, ProductCode) VALUES ("5400141902389","AAM-BL-0BL604RMF")');
  });
})     

starter.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })

  .state('mainPage', {
      url: '/mainPage',
      templateUrl: 'templates/mainPage.html',
      controller: 'mainPageCtrl'
  })

  .state('dashboard', {
      url: '/dashboard',
      templateUrl: 'templates/dashboard.html',
      controller: 'mainPageCtrl'
      
  })


  // setup an abstract state for the tabs directive
   /* .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })*/

  // Each tab has its own nav history stack:

  /*.state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })*/


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
